﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EVE.SingleSignOn.Core;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RoleBasedAccessControl.Models;
using RoleBasedAccessControl.Models.Settings;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RoleBasedAccessControl.Controllers
{
    public class LoginController : Controller
    {
        private readonly SsoConfig _ssoConfig;
        private readonly ISingleSignOnClient _sso;
        private readonly Ctx _db;

        public LoginController(IOptions<SsoConfig> ssoConfig, Ctx db)
        {
            _db = db;
            _ssoConfig = ssoConfig.Value;
            _sso = new SingleSignOnClient(GetEveSettings());
        }

        private SsoSettings GetEveSettings()
        {
            var settings = new SsoSettings()
            {
                BaseUrl = _ssoConfig.BaseUrl,
                CallbackUrl = _ssoConfig.CallbackUrl,
                ClientId = _ssoConfig.ClientId,
                ClientSecret = _ssoConfig.ClientSecret,
                ContactEmail = _ssoConfig.ContactEmail,
                Scope = _ssoConfig.Scope,
                State = Guid.NewGuid().ToString()
            };

            return settings;
        }


        public IActionResult Login()
        {
            if (!User.Identity.IsAuthenticated)
                return Redirect(_sso.GetAuthenticationUrl());

            return RedirectToAction("About", "Home");
        }

        public async Task<IActionResult> Callback(string code, string state)
        {
            // Authenticate using the code we received from SSO
            // so let's get some tokens
            var tokens = await _sso.AuthenticateAsync(code);

            // Store the tokens in session
            var character = await _sso.VerifyAsync(tokens.AccessToken);

            // Save the character if they are not already in the DB
            var user = await _db.Users.FirstOrDefaultAsync(u => u.CharacterOwnerHash == character.CharacterOwnerHash);

            if (user == null)
            {
                var newUser = new User()
                {
                    CharacterOwnerHash = character.CharacterOwnerHash,
                    CharacterId = character.CharacterId,
                    CharacterName = character.CharacterName,
                    LockoutEnabled = false,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                };

                // Add a standard user role
                var roles = await _db.Roles.ToListAsync();
                var roleUser = new List<RoleUser>();
                var ru = new RoleUser()
                {
                    User = newUser,
                    UserId = newUser.UserId,
                    Role = roles.FirstOrDefault(role => role.IsDefaultRole == true),
                    RoleId = roles.FirstOrDefault(role => role.IsDefaultRole == true)?.RoleId
                };
                roleUser.Add(ru);
                newUser.RoleUser = roleUser;

                await _db.Users.AddAsync(newUser);
                await _db.SaveChangesAsync();
            }

            // Get any roles the user may have
            var userRole = _db.Users.Include(ru => ru.RoleUser).ThenInclude(r => r.Role).First(u => u.CharacterOwnerHash == character.CharacterOwnerHash);

            // Add claims
            var claims = new List<Claim>
            {
                new Claim("roles", JsonConvert.SerializeObject(userRole,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        })),
                new Claim(ClaimTypes.NameIdentifier, character.CharacterOwnerHash),
                new Claim(ClaimTypes.Name, character.CharacterName),
                new Claim(ClaimTypes.SerialNumber, character.CharacterId.ToString()),
                new Claim(ClaimTypes.UserData, JsonConvert.SerializeObject(character)),
                new Claim("tokens", JsonConvert.SerializeObject(tokens)),
                new Claim("tokenExpiry", JsonConvert.SerializeObject(DateTime.UtcNow.AddSeconds(tokens.ExpiresIn))),
                new Claim("canRefresh", JsonConvert.SerializeObject(!string.IsNullOrEmpty(tokens.RefreshToken)))
            };

            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var claimsPrinciple = new ClaimsPrincipal(claimsIdentity);

            // Sign User in
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
                                            claimsPrinciple,
                                            new AuthenticationProperties
                                            {
                                                IsPersistent = true
                                            });


            return RedirectToAction("About", "Home");
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync("Cookies");
            return RedirectToAction("Index", "Home");
        }
    }
}
