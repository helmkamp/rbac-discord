﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RoleBasedAccessControl.Models
{
    public class User
    {
        public User()
        {
            UpdatedAt = DateTime.Now;
        }

        [Key]
        public long UserId { get; set; }
        [Display(Name = "Character Owner Hash")]
        public string CharacterOwnerHash { get; set; }
        [Display(Name = "Character ID")]
        public long CharacterId { get; set; }
        [Display(Name = "CharacterName")]
        public string CharacterName { get; set; }
        [Display(Name = "Discord ID")] 
        public string DiscordId { get; set; }
        [Display(Name = "Lockout")]
        public bool LockoutEnabled { get; set; }
        [Display(Name = "Created At")]
        [DataType(DataType.Date)]
        public DateTime CreatedAt { get; set; }
        [Display(Name = "Last Updated")]
        [DataType(DataType.Date)]
        public DateTime UpdatedAt { get; set; }

        public List<RoleUser> RoleUser { get; set; }
    }
}
