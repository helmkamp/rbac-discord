﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace RoleBasedAccessControl.Models
{
    public class Role
    {
        [Key]
        public string RoleId { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [StringLength(75)]
        [Display(Name = "Description")]
        public string RoleDescription { get; set; }
        [Display(Name = "Discord Roles")]
        public List<SelectListItem> DiscordRoles { get; set; }
        [Display(Name = "Discord Role")]
        public string DiscordRole { get; set; }
        [Display(Name = "System Admin")]
        public bool IsSysAdmin { get; set; }
        public bool IsDefaultRole { get; set; }

        public List<RoleUser> RoleUser { get; set; }
        public List<PermissionRole> PermissionRole { get; set; }
    }
    
    public class RoleViewModel
    {
        public string Role { get; set; }
        public List<SelectListItem> Roles { get; set; }
        public long UserId { get; set; }
    }

    public class RequiredRole
    {
        public string Role { get; set; }
    }
}
