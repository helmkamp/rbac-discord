﻿namespace RoleBasedAccessControl.Models
{
    public class RoleUser
    {
        public long UserId { get; set; }
        public User User { get; set; }

        public string RoleId { get; set; }
        public Role Role { get; set; }
    }
}
