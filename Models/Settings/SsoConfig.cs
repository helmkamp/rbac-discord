﻿namespace RoleBasedAccessControl.Models.Settings
{
    public class SsoConfig
    {
        public string BaseUrl { get; set; }
        public string CallbackUrl { get; set; }
        public string Scope { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string ContactEmail { get; set; }
        public string State { get; set; }
    }
}
