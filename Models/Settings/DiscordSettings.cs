namespace RoleBasedAccessControl.Models.Settings
{
    public class DiscordSettings
    {
        public string BaseUrl { get; set; }
        public string GuildId { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string RedirectUrl { get; set; }
        public string BotToken { get; set; }
        public string BotPermissions { get; set; }
    }
}