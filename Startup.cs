﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RoleBasedAccessControl.Models;
using RoleBasedAccessControl.Models.Settings;
using RoleBasedAccessControl.Policies;

namespace RoleBasedAccessControl
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => false;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(options =>
            {
                options.LoginPath = new PathString("/Login/Login");
                options.LogoutPath = new PathString("/Login/Logout");
                options.AccessDeniedPath = new PathString("/Home/Error");
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("DynamicPermissions",
                    policy => policy.Requirements.Add(new DynamicPermissionRequirement()));
                options.AddPolicy("DynamicMenu",
                    policy => policy.Requirements.Add(new DynamicMenuRequirement()));
                options.AddPolicy("IsSysAdmin", policy => policy.Requirements.Add(new IsSysAdminRequirement()));
            });

            services.Configure<SsoConfig>(Configuration.GetSection("SingleSignOn"));
            services.Configure<DiscordSettings>(Configuration.GetSection("DiscordSettings"));

            services.AddDbContext<Ctx>(
                options => options.UseSqlServer(Configuration.GetConnectionString("MsSql"))
            );

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddScoped<IAuthorizationHandler, DynamicPermissionHandler>();
            services.AddScoped<IAuthorizationHandler, DynamicMenuHandler>();
            services.AddScoped<IAuthorizationHandler, IsSysAdminHandler>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                
            }
            else
            {
                app.UseExceptionHandler("/Main/Home/Error");
                app.UseHsts();
            }

            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "areaRoute",
                    template: "{area:exists}/{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
