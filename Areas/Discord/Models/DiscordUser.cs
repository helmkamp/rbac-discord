using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace RoleBasedAccessControl.Areas.Discord.Models
{
    public class DiscordUser
    {
        [JsonProperty(PropertyName = "username")]
        public string Username { get; set; }
        [JsonProperty(PropertyName = "locale")]
        public string Locale { get; set; }
        [JsonProperty(PropertyName = "mfa_enabled")]
        public bool MfaEnabled { get; set; }
        [JsonProperty(PropertyName = "avatar")]
        public string Avatar { get; set; }
        [JsonProperty(PropertyName = "discriminator")]
        public string Discriminator { get; set; }
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
    }
}


//{
         //"username": "helmy",
         //"locale": "en-US",
         //"mfa_enabled": false,
         //"avatar": "d87ee8b331e6132d1b3f388b86b59a2e",
         //"discriminator": "8646",
         //"id": "195329349451382784"
         //}