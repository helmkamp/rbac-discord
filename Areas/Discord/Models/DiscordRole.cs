using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace RoleBasedAccessControl.Areas.Discord.Models
{
    public class DiscordRole
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
//        [JsonProperty(PropertyName = "color")]
//        public int Color { get; set; }
//        [JsonProperty(PropertyName = "hoist")]
//        public bool Hoist { get; set; }
//        [JsonProperty(PropertyName = "position")]
//        public int Position { get; set; }
//        [JsonProperty(PropertyName = "permissions")]
//        public int Permissions { get; set; }
//        [JsonProperty(PropertyName = "managed")]
//        public bool Managed { get; set; }
//        [JsonProperty(PropertyName = "mentionable")]
//        public bool Mentionable { get; set; }
    }

    public class DiscordRoles
    {
        public List<DiscordRole> Roles { get; set; }
    }
}