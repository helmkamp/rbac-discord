using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RestSharp;
using RoleBasedAccessControl.Areas.Discord.Models;
using RoleBasedAccessControl.Models;
using RoleBasedAccessControl.Models.Settings;

namespace RoleBasedAccessControl.Areas.Discord.Controllers
{
    [Area("Discord")]
    [Authorize(Policy = "DynamicPermissions")]
    public class InviteController : Controller
    {
        private readonly Ctx _ctx;
        private readonly DiscordSettings _discordSettings;

        public InviteController(Ctx ctx, IOptions<DiscordSettings> discordSettings)
        {
            _ctx = ctx;
            _discordSettings = discordSettings.Value;
        }
        // GET
        public IActionResult Index() => View();

        public IActionResult GuildInvite()
        {
            return Redirect(GetDiscordAuthCode());
        }
        
        public string GetDiscordAuthCode()
        {
            return
                $"https://discordapp.com/api/oauth2/authorize?client_id={_discordSettings.ClientId}&redirect_uri={_discordSettings.RedirectUrl}&response_type=code&scope=guilds.join%20identify";
        }
        
        [AllowAnonymous]
        public IActionResult GetDiscordAuth(string code)
        {
            var webRequest = (HttpWebRequest) WebRequest.Create($"{_discordSettings.BaseUrl}oauth2/token");
            webRequest.Method = "POST";
            var parameters =
                $"client_id={_discordSettings.ClientId}&client_secret={_discordSettings.ClientSecret}&grant_type=authorization_code&code={code}&redirect_uri={_discordSettings.RedirectUrl}";
            var byteArray = Encoding.UTF8.GetBytes(parameters);
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.ContentLength = byteArray.Length;
            var postStream = webRequest.GetRequestStream();
            
            postStream.Write(byteArray, 0, byteArray.Length);
            postStream.Close();
            var response = webRequest.GetResponse();
            postStream = response.GetResponseStream();
            var reader = new StreamReader(postStream);
            var responseFromServer = JsonConvert.DeserializeObject<DiscordAuthResponse>(reader.ReadToEnd());

            GetUserInfo(responseFromServer);

            return RedirectToAction("Index");
        }

        private void GetUserInfo(DiscordAuthResponse responseFromServer)
        {
            var webRequest = (HttpWebRequest) WebRequest.Create($"{_discordSettings.BaseUrl}users/@me");
            webRequest.Method = "GET";
            webRequest.ContentLength = 0;
            webRequest.Headers.Add("Authorization", $"Bearer {responseFromServer.AccessToken}");
            webRequest.ContentType = "application/x-www-form-urlencoded";

            DiscordUser user;
            using (var response = webRequest.GetResponse() as HttpWebResponse)
            {
                var reader = new StreamReader(response.GetResponseStream());
                user = JsonConvert.DeserializeObject<DiscordUser>(reader.ReadToEnd());
            }

            UpdateCorpUser(user);
            InviteUser(user, responseFromServer);
        }

        private void UpdateCorpUser(DiscordUser user)
        {
            var currentUser = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value;
            var corpUser = _ctx.Users.FirstOrDefault(x => x.CharacterOwnerHash == currentUser);

            corpUser.DiscordId = user.Id;

            _ctx.Update(corpUser);
            _ctx.SaveChanges();
        }

        private void InviteUser(DiscordUser user, DiscordAuthResponse authResponse)
        {
            var client = new RestClient(_discordSettings.BaseUrl);
            var request = new RestRequest($"guilds/{_discordSettings.GuildId}/members/{user.Id}", Method.PUT);
            var accessToken = new {access_token = authResponse.AccessToken};
            request.AddHeader("Authorization", "Bot " + _discordSettings.BotToken);
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(accessToken);
            
            var response = client.Execute(request);
            var content = response.Content;
        }
    }
}