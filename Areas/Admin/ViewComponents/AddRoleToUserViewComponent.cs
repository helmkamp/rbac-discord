using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RoleBasedAccessControl.Models;

namespace RoleBasedAccessControl.Areas.Admin.ViewComponents
{
    public class AddRoleToUserViewComponent : ViewComponent
    {
        private readonly Ctx _ctx;

        public AddRoleToUserViewComponent(Ctx ctx)
        {
            _ctx = ctx;
        }

        public async Task<IViewComponentResult> InvokeAsync(long id)
        {
            var roles = await _ctx.Roles.ToListAsync();
            var user = await _ctx.Users.Include(ru => ru.RoleUser).ThenInclude(r => r.Role)
                .FirstOrDefaultAsync(u => u.UserId == id);
            
            var unusedRoles = new List<SelectListItem>();
            foreach (var role in roles)
            {
                if (user.RoleUser.Exists(r => r.Role == role)) continue;
                
                var temp = new SelectListItem()
                {
                    Value = role.RoleId,
                    Text = role.Name
                };
                
                unusedRoles.Add(temp);
            }
            
            
            var result = new RoleViewModel()
            {
                Roles = unusedRoles,
                UserId = id
            };

            return View(result);
        }
    }
}