using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RoleBasedAccessControl.Models;

namespace RoleBasedAccessControl.Areas.Admin.ViewComponents
{
    public class ListPermissionsViewComponent : ViewComponent
    {
        private readonly Ctx _ctx;

        public ListPermissionsViewComponent(Ctx ctx)
        {
            _ctx = ctx;
        }

        public async Task<IViewComponentResult> InvokeAsync(string id)
        {
            var role = await GetRoleAsync(id);

            return View(role);
        }

        private Task<Role> GetRoleAsync(string id)
        {
            return _ctx.Roles.Include(pr => pr.PermissionRole).ThenInclude(p => p.Permission).FirstAsync(r => r.RoleId == id);
        }
    }
}