using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RoleBasedAccessControl.Models;

namespace RoleBasedAccessControl.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Policy = "DynamicPermissions")]
    public class PermissionsController : Controller
    {
        private readonly Ctx _ctx;
        
        public PermissionsController(Ctx ctx)
        {
            _ctx = ctx;
        }
        
        // GET
        public async Task<IActionResult> Index()
        {
            return View(await _ctx.Permissions.ToListAsync());
        }
        
        public async Task<IActionResult> Details(int id)
        {
            var permission = await _ctx.Permissions.FindAsync(id);
            if (permission == null)
            {
                return NotFound();
            }
            return View(permission);
        }
        
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Permission permission)
        {
            if (ModelState.IsValid)
            {
                await _ctx.Permissions.AddAsync(permission);
                await _ctx.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(permission);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var permission = await _ctx.Permissions.FindAsync(id);
            if (permission == null)
            {
                return NotFound();
            }
            return View(permission);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Permission permission)
        {
            if (ModelState.IsValid)
            {
                _ctx.Update(permission);
                await _ctx.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(permission);
        }

        public async Task<IActionResult> Delete(int id)
        {
            var permission = await _ctx.Permissions.FindAsync(id);
            if (permission == null)
            {
                return NotFound();
            }
            return View(permission);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmation(int id)
        {
            var permission = await _ctx.Permissions.FindAsync(id);
            _ctx.Permissions.Remove(permission);
            await _ctx.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddPermissionToRole(PermissionViewModel model)
        {
            Role role = await _ctx.Roles.Include(pr => pr.PermissionRole).ThenInclude(p => p.Permission).FirstAsync(r => r.RoleId == model.RoleId);
            Permission permission = await _ctx.Permissions
                .FirstOrDefaultAsync(p => p.PermissionId == Convert.ToInt32(model.Permission));

            if (!role.PermissionRole.Exists(p => p.Permission == permission))
            {
                var result = new PermissionRole()
                {
                    Permission = permission,
                    PermissionId = permission.PermissionId,
                    Role = role,
                    RoleId = model.RoleId
                };
                
                role.PermissionRole.Add(result);
                _ctx.Update(role);
                await _ctx.SaveChangesAsync();
            }

            return RedirectToAction("Edit", "Roles", new { id = model.RoleId});
        }

        public async Task<IActionResult> AddAllPermissionsToRole(string roleId)
        {
            var permissions = await _ctx.Permissions.ToListAsync();
            var rolePermissions = await _ctx.Roles.Include(pr => pr.PermissionRole)
                .ThenInclude(p => p.Permission)
                .FirstAsync(r => r.RoleId == roleId);
            
           
            foreach (var permission in permissions)
            {
                if (rolePermissions.PermissionRole.Exists(p => p.Permission == permission)) continue;
                var temp = new PermissionRole()
                {
                    Permission = permission,
                    PermissionId = permission.PermissionId,
                    Role = rolePermissions,
                    RoleId = rolePermissions.RoleId
                };
                rolePermissions.PermissionRole.Add(temp);
            }

            await _ctx.SaveChangesAsync();

            return RedirectToAction("Edit", "Roles", new { id = roleId});
        }

        public async Task<IActionResult> DeletePermissionFromRole(string roleId, int permissionId)
        {
            var role = await _ctx.Roles.Include(pr => pr.PermissionRole).ThenInclude(p => p.Permission)
                .FirstAsync(r => r.RoleId == roleId);
            var perm = role.PermissionRole.FirstOrDefault(p => p.PermissionId == permissionId);
            
            role.PermissionRole.Remove(perm);
            await _ctx.SaveChangesAsync();
            
            return RedirectToAction("Edit", "Roles", new { id = roleId});
        }
    }
}