﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RoleBasedAccessControl.Areas.Discord.Models;
using RoleBasedAccessControl.Models;
using RoleBasedAccessControl.Models.Settings;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RoleBasedAccessControl.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Policy = "DynamicPermissions")]
    public class RolesController : Controller
    {
        private readonly Ctx _ctx;
        private List<DiscordRole> _discordRoles;
        private readonly DiscordSettings _discordSettings;
        
        public RolesController(Ctx ctx, IOptions<DiscordSettings> discordSettings)
        {
            _ctx = ctx;
            _discordSettings = discordSettings.Value;
        }
        // GET: /<controller>/
        public async Task<IActionResult> Index()
        {
            var roles = await _ctx.Roles.ToListAsync();
            return View(roles);
        }

        public IActionResult Create()
        {
            GetRoles();
            var roles = new List<SelectListItem>();
            foreach (var discordRole in _discordRoles)
            {
                var temp = new SelectListItem()
                {
                    Text = discordRole.Name,
                    Value = discordRole.Id
                };
                roles.Add(temp);
            }
            
            var result = new Role()
            {
                DiscordRoles = roles
            };
            return View(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Role role)
        {
            if (ModelState.IsValid)
            {
               await _ctx.Roles.AddAsync(role);
               await _ctx.SaveChangesAsync();
               return RedirectToAction("Index");
            }

            return View(role);
        }

        [HttpGet]
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            var role = await _ctx.Roles.FindAsync(id);
            if (role == null)
            {
                return NotFound();
            }
            return View(role);
        }
        
        [HttpGet]
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            var role = await _ctx.Roles.FindAsync(id);
            if (role == null)
            {
                return NotFound();
            }
            
            GetRoles();
            var roles = new List<SelectListItem>();
            foreach (var discordRole in _discordRoles)
            {
                var temp = new SelectListItem()
                {
                    Text = discordRole.Name,
                    Value = discordRole.Id
                };
                roles.Add(temp);
            }

            role.DiscordRoles = roles;
            
            return View(role);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Role role)
        {
            if (ModelState.IsValid)
            {
                _ctx.Update(role);
                await _ctx.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(role);
        }

        [HttpGet]
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            var role = await _ctx.Roles.FindAsync(id);
            if (role == null)
            {
                return NotFound();
            }
            return View(role);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var role = await _ctx.Roles.FindAsync(id);
            _ctx.Roles.Remove(role);
            await _ctx.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddUserToRole(RoleViewModel model)
        {
            var user = await _ctx.Users.Include(ru => ru.RoleUser).ThenInclude(r => r.Role)
                .FirstOrDefaultAsync(u => u.UserId == model.UserId);
            var role = await _ctx.Roles.FirstOrDefaultAsync(r => r.RoleId == model.Role);

            if (user.RoleUser.Exists(r => r.RoleId == model.Role))
                return RedirectToAction("Edit", "Users", new {id = model.UserId});
            
            var newRole = new RoleUser()
            {
                Role = role,
                RoleId = model.Role,
                UserId = model.UserId,
                User = user
            };
            user.RoleUser.Add(newRole);
            _ctx.Update(user);
            await _ctx.SaveChangesAsync();

            await AddUserToDiscordRole(user.DiscordId, model.Role);

            return RedirectToAction("Edit", "Users", new {id = model.UserId});
        }

        public async Task<IActionResult> DeleteUserFromRole(int id, string roleId)
        {
            var user = await _ctx.Users.Include(ru => ru.RoleUser).ThenInclude(r => r.Role)
                .FirstOrDefaultAsync(u => u.UserId == id);
            var role = user.RoleUser.FirstOrDefault(r => r.RoleId == roleId);

            user.RoleUser.Remove(role);
            await _ctx.SaveChangesAsync();
            await RemoveUserFromDiscordRole(user.DiscordId, roleId);
            return RedirectToAction("Edit", "Users", new {id = id});
        }
        
        private void GetRoles()
        {
            var webRequest =
                (HttpWebRequest) WebRequest.Create($"https://discordapp.com/api/guilds/{_discordSettings.GuildId}/roles");
            webRequest.Method = "GET";
            webRequest.ContentLength = 0;
            webRequest.Headers.Add("Authorization", "Bot " + _discordSettings.BotToken);
            webRequest.ContentType = "application/x-www-form-urlencoded";
            
            List<DiscordRole> roles;
            using (var response = webRequest.GetResponse() as HttpWebResponse)
            {
                if (response == null)
                    throw new NullReferenceException();
                
                var reader = new StreamReader(response.GetResponseStream() ?? throw new NullReferenceException());
                roles = JsonConvert.DeserializeObject<List<DiscordRole>>(reader.ReadToEnd());
            }

            _discordRoles = roles;
        }
        
        private async Task AddUserToDiscordRole(string user, string roleId)
        {
            var discordRoleId = await _ctx.Roles.FirstOrDefaultAsync(r => r.RoleId == roleId);
            var webRequest =
                (HttpWebRequest) WebRequest.Create($"https://discordapp.com/api/guilds/{_discordSettings.GuildId}/members/{user}/roles/{discordRoleId.DiscordRole}");
            webRequest.Method = "PUT";
            webRequest.ContentLength = 0;
            webRequest.Headers.Add("Authorization", "Bot " + _discordSettings.BotToken);
            webRequest.ContentType = "application/x-www-form-urlencoded";
            
            
            using (var response = webRequest.GetResponse() as HttpWebResponse)
            {
                if (response == null)
                    throw new NullReferenceException();
                
                var reader = new StreamReader(response.GetResponseStream() ?? throw new NullReferenceException());
            }
        }
        
        private async Task RemoveUserFromDiscordRole(string user, string roleId)
        {
            var discordRoleId = await _ctx.Roles.FirstOrDefaultAsync(r => r.RoleId == roleId);
            var webRequest =
                (HttpWebRequest) WebRequest.Create($"https://discordapp.com/api/guilds/{_discordSettings.GuildId}/members/{user}/roles/{discordRoleId.DiscordRole}");
            webRequest.Method = "DELETE";
            webRequest.ContentLength = 0;
            webRequest.Headers.Add("Authorization", "Bot " + _discordSettings.BotToken);
            webRequest.ContentType = "application/x-www-form-urlencoded";
            
            
            using (var response = webRequest.GetResponse() as HttpWebResponse)
            {
                if (response == null)
                    throw new NullReferenceException();
                
                var reader = new StreamReader(response.GetResponseStream() ?? throw new NullReferenceException());
            }
        }
    }
}
